from contextlib import asynccontextmanager
from functools import partial
import strawberry
from strawberry.types import Info
from fastapi import FastAPI
from strawberry.fastapi import BaseContext, GraphQLRouter
from databases import Database
from pypika.dialects import PostgreSQLQuery as PQuery, Table

from settings import Settings


class Context(BaseContext):
    db: Database

    def __init__(
        self,
        db: Database,
    ) -> None:
        self.db = db



@strawberry.type
class Author:
    name: str


@strawberry.type
class Book:
    title: str
    author: Author


BookTable = Table("books")
AuthorTable = Table("authors")


@strawberry.type
class Query:

    @strawberry.field
    async def books(
        self,
        info: Info[Context, None],
        author_ids: list[int] | None = None,
        search: str | None = None,  # Фуннкциональные требования?
        limit: int | None = None,
    ) -> list[Book]:
        # Do NOT use dataloaders
        query = (
            PQuery.from_(BookTable)
            .join(AuthorTable).on(BookTable.author_id == AuthorTable.id)
            .select(BookTable.title, AuthorTable.name)
        )
        if author_ids:
            query = query.where(AuthorTable.field('id').isin(author_ids))

        if limit:
            query = query.limit(limit)

        data = await info.context.db.fetch_all(query=str(query))
        res = [Book(title=i.title, author=Author(name=i.name)) for i in data]  # type: ignore
        return res


CONN_TEMPLATE = "postgresql+asyncpg://{user}:{password}@{host}:{port}/{name}"
settings = Settings()  # type: ignore
db = Database(
    CONN_TEMPLATE.format(
        user=settings.DB_USER,
        password=settings.DB_PASSWORD,
        port=settings.DB_PORT,
        host=settings.DB_SERVER,
        name=settings.DB_NAME,
    ),
)

@asynccontextmanager
async def lifespan(
    app: FastAPI,
    db: Database,
):
    async with db:
        yield

schema = strawberry.Schema(query=Query)
graphql_app = GraphQLRouter(  # type: ignore
    schema,
    context_getter=partial(Context, db),
)

app = FastAPI(lifespan=partial(lifespan, db=db))
app.include_router(graphql_app, prefix="/graphql")
